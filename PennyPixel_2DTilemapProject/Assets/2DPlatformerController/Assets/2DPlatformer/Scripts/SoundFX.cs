﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is for the background music. I wanted it to loop of course,
// but I didn't want it to be interrupted by loading a new scene, so I made this. 

// I found the awesome song here: 
// https://files.freemusicarchive.org/storage-freemusicarchive-org/music/Creative_Commons/Ketsa/Raising_Frequecy/Ketsa_-_09_-_Life_Illusion.mp3

public class SoundFX : MonoBehaviour
{
    private static SoundFX soundInstance;
    private void Awake()
    {
        //Tells Unity to not get rid of this when I load a new scene.
        DontDestroyOnLoad(this);

        // So, the first scene contains this sound game object which can't be destroyed by
        // loading a scene. But, if you die in the first scene, the first scene is reloaded.
        // This means I have to do something to stop a bunch of these from amassing if you die
        // in the first scene, because this won't be destroyed and a new version of it will be 
        // loaded.

        //This block just says "If I exist already, destroy me. If I don't, let me be."
        if (soundInstance == null)
        {
            soundInstance = this;
        }
        else
        {
            Object.Destroy(this.gameObject);
        }
    }
}
