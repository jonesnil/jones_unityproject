﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lava : MonoBehaviour
{
    public GameObject LavaDeathEffect;
    public Image image;
    public AudioSource sizzling;
    protected Rigidbody2D rb2d;
    private bool hitLava;

    // Start is called before the first frame update
    void Start()
    {
        //Grabbing all my components
        this.rb2d = this.GetComponent<Rigidbody2D>();
        LavaDeathEffect = GameObject.Find("LavaDeathEffect");
        image = LavaDeathEffect.transform.GetChild(0).GetComponent<Image>();
        hitLava = false;
        sizzling = transform.GetComponent<AudioSource>();
    }

    private void Update()
    {
        // If you hit the lava and the screen isn't red, fade the screen red with Lerp.
        // This is pretty much exactly how I fade to purple for the time effect in Player
        // Controller.
        if(image.color.a < .98f && hitLava)
        {
            Color tempColor = image.color;
            tempColor.a = Mathf.Lerp(image.color.a, 1f, 25f * Time.deltaTime * .5f);
            image.color = tempColor;
        }

        // If you hit lava and you already faded the screen, set time back to normal and reload
        // the scene you're on so the player can try again.
        if (image.color.a > .98f && hitLava)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // This plays a cool sizzling sound I found here: https://freesound.org/people/JasonElrod/sounds/85468/
        // only if you touch lava. It also sets the bool hitLava to true, so the red image can slowly fade up.
        // It only does this if you haven't already hit lava, though, because I don't want the effects to compound
        // at all. Oh, and it makes thing move in slow motion for a second to make it more dramatic.
        if (other.tag == "Player" && !hitLava)
        {
            sizzling.Play();
            hitLava = true;
            Time.timeScale = .2f;
        }
    }

}
