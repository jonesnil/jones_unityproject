﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

//This script is for the exit sign
public class Exit : MonoBehaviour
{
    private Scene currentScene;
    private string sceneName;
    private string nextSceneName;
    private int currentLevel;
    protected Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        //This just grabs the scene we're in and gets its name.
        this.currentScene = SceneManager.GetActiveScene();
        this.sceneName = currentScene.name;

        // All the levels (scenes) are called Level"X" with "X" being the level number. This just
        // grabs that "X" so I know what level the player is on.
        currentLevel = Convert.ToInt32(sceneName.Substring(sceneName.Length - 1));

        // This takes that level, adds 1 and puts it into the Level"X" format so it
        // knows what the next scene is called.
        nextSceneName = String.Format("Level{0}", currentLevel + 1);

        this.rb2d = this.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Checks if the player touched the exit
        if (other.tag == "Player")
        {
            //If this is true the next level exists
            if (currentLevel + 1 <= SceneManager.sceneCountInBuildSettings)
                //If the next level exists, load it.
                SceneManager.LoadScene(nextSceneName, LoadSceneMode.Single);

            //If the next level doesn't exist, we must be at the end of the game, so quit.
            else
            {
                Application.Quit();
            }
        }
    }
}
