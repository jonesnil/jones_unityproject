﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.SceneManagement;

public class PlayerPlatformerController : Physics_Object
{
    // This keeps track of the time between successful jumps between levels, to fix a weird
    // bug I'll explain later.
    public float jumpBufferTime;

    // This is where I will hold the time-warp jump sound effect. I decided to use
    // this very cool earthquake type sound from here: 
    // https://freesound.org/people/ryanconway/sounds/248343/
    public AudioSource jumpSound;

    // These are the objects that hold my two different maps for each level.
    // They are actually just empty holders for Grid objects, but I found that
    // if I used the grid itself here it would throw null errors, so I put in 
    // the place holders.

    public GameObject past;
    public GameObject present;

    //Only used to toggle the different level backgrounds (with their clouds).
    public GameObject pastBackground;
    public GameObject presentBackground;

    // It turns out Cinemachine has camera shake built in, so these variables
    // will let this script change the shake values to make the screen shake when
    // the player jumps.

    public CinemachineVirtualCamera vCamera;
    public CinemachineBasicMultiChannelPerlin shake;

    public GameObject openingText;
    public GameObject timeJumpFX;
    public Image image;
    public Text gameOver;
    public Text jumpsLeftText;
    public int jumpsLeft;
    public int maxJumps;
    public bool dead;
    public bool textClosed;

    public float maxSpeed = 7f;
    public float jumpTakeOffSpeed = 7f;
        
    // These booleans are just used internally to keep track of whether purple should
    // be fading in or fading out. If they're both false the purple fade isn't happening,
    // and they should never both be true.

    private bool jumpFXup;
    private bool jumpFXdown;

    // Added this so I can make the fade go quick when you don't actually jump through time. Just a way
    // to signal the player they couldn't switch levels.
    private float fadeSpeed;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    // Start is called before the first frame update
    void Awake()
    {
        dead = false;

        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        //Just grabbing the references for the grids
        past = GameObject.Find("PastReference");
        present = GameObject.Find("PresentReference");
        presentBackground = GameObject.Find("PresentBackground");
        pastBackground = GameObject.Find("PastBackground");

        //Grabbing the jump sound
        jumpSound = transform.GetComponents<AudioSource>()[0];

        //Setting up the shake variable (I wanted to set the vCamera here too just because I like
        // the cleanness of doing it in a script, but I couldn't find a way to do it as a cinemachine thing.
        // I could've done it as a game object but then I couldn't call the next thing.)

        shake = vCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();

        //Getting things ready for my visual effects.

        jumpFXup = false;
        jumpFXdown = false;
        timeJumpFX = GameObject.Find("TimeJumpEffect");
        openingText = GameObject.Find("OpeningText");
        textClosed = false;

        // This is what it should usually be, it changes if you didn't actually switch levels.
        fadeSpeed = 1.25f;

        // This is a long line kinda but it just grabs the image from the panel under timeJumpFX,
        // which contains the color I need to change the transparency of.

        image = timeJumpFX.transform.GetChild(0).gameObject.GetComponent<Image>();
        jumpsLeftText = timeJumpFX.transform.GetChild(1).gameObject.GetComponent<Text>();

        gameOver = timeJumpFX.transform.GetChild(2).GetComponent<Text>();

        jumpsLeft = maxJumps;
        string jumps = jumpsLeft.ToString();
        jumpsLeftText.text = string.Format("Jumps Remaining: {0}", jumps);

        // Naturally, this starts at 0, since no time has elapsed. It doesn't
        // matter really what it is though, because it'll be reset before it's
        // used anywhere.
        jumpBufferTime = 0f;
    }



    // This function is only called if one of the jumpFX bools are true, and it will fade
    // up to half opaqueness of the purple image and then down back to 0. It fades much
    // quicker if you didn't switch levels due to collision.

    private void JumpFX()
    {
        // I may have gone overboard with this math a bit but hopefully I can explain it. 
        // Basically I want the purple to be more opaque as you have fewer jumps, to give 
        // a cool tension where you realize you're almost out of them. But, the max jumps
        // isn't always the same number, and I'm way too lazy to map this to different numbers
        // with if statements. So, instead, I take maxJumps/jumpsLeft and put it into a sigmoid
        // function which will map so that lower jumps left = closer to 0 and higher jumps left
        // is closer to 1. 
        //
        // The -.23 is just because if you put one into the sigmoid (which is always close to the
        // first result, because jumpsleft is almost maxjumps at that point) you get .73 something, 
        // but I want the effect to start at .5. There's a better way to fix that using coefficients
        // I think but this works out to gradually go up to like .80something or so before you run
        // out of jumps. 

        float jumpRelation = ((float)maxJumps) / ((float)jumpsLeft);
        float purpleOpacity = (1f / (1f + Mathf.Exp(-jumpRelation))) - .23f;

        //So if jumpFXup is true we want to slowly fade up to half purple visibility.
        if (jumpFXup)
        {
            //image.color.a is just the opaqueness of the color.
            if (image.color.a < purpleOpacity)
            {
                // I have to use tempColor here and in the other one because for some reason
                // you aren't allowed to change image.color.a directly, you have to make a copy
                // change it and send it back.

                Color tempColor = image.color;

                // Lerp again, the most convenient method for lazy effects ever. Also,
                // the deltaTime there is of course to make the fade not frame rate dependent.

                // Purple opacity is in this so that when the purple gets more opaque it still takes the 
                // same (or close) amount of time to fade back. Otherwise the pulses of purple on the screen
                // get really long and that's not the effect I want. The fade down in the next bit does the same
                // thing.
                tempColor.a = Mathf.Lerp(image.color.a, 1, fadeSpeed * Time.deltaTime * (purpleOpacity + .5f));
                image.color = tempColor;
            }

            else
            {
                // So if we get in this else purple has faded up and it's time to fade it 
                // back down, so we turn on jumpFXdown and turn jumpFXup off.
                jumpFXup = false;
                jumpFXdown = true;
            }
        }

        //This is the same as what we did in the last if statement, but we are fading the opaqueness
        // back down instead of up. 

        //The fact that it's an else if is important because if you manage to jump again before the fade 
        // stops you can end up with both if statements going at once which is not good and ruins the look 
        // of the game (constant purple.) This fixes it so the fade will just continue from wherever it was 
        // when you jumped again. 

        else if (jumpFXdown)
        {
            // This is set to be .02 and not 0 because it takes a long time to reach 0 with this Lerp
            // and I'd prefer this stops quickly. .02 is pretty much the same as 0, I don't think the 
            // leftover purple is noticable.

            if (image.color.a > .02f)
            {
                Color tempColor = image.color;
                tempColor.a = Mathf.Lerp(image.color.a, 0, fadeSpeed * Time.deltaTime * (purpleOpacity + .5f));
                image.color = tempColor;
            }

            else
            {
                jumpFXup = false;
                jumpFXdown = false;

                // This is to reset the speed in case you didn't fade right this time due to collision
                // with other level.
                fadeSpeed = 1.25f;
            }
        }



    }

    // This is only called when you lose, of course, but it's called every frame
    // so technically the game still runs in the background. It just covers your
    // screen with purple so you can't tell.

    private void GameOver()
    {
        shake.m_AmplitudeGain = 2f;
        //This is the same as the old fade for time jump, just doesn't fade back.   
        if (image.color.a < 1f)
        {
            Color tempColor = image.color;
            tempColor.a = Mathf.Lerp(image.color.a, 1, fadeSpeed * Time.deltaTime);
            image.color = tempColor;
        }

        // Message should only show when the screen is basically opaque.
        if (image.color.a > .90f)
        {
            gameOver.text = "TIME UNKNOWN";
            // After a second and a half it will restart the level.
            Invoke("Restart", 1.5f);
        }
    }

    // Just a simple method to restart the level. Made it a function so I could call
    // invoke on it and easily put it on a timer for the GameOver function.
    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    //This is called every frame so pretty much all my changes are called in this, for convenience.

    protected override void ComputeVelocity()
    {
        // I'm lazy so I always have jumpBufferTime adding up here. If I wanted to optimize it
        // it technically only needs to keep track when the player is in the air during a successful
        // level switch, though.
        jumpBufferTime += Time.deltaTime;

        // This just lets the player close the opening text for a level by pressing a button.

        // It returns after because I found in playtesting people would get frustrated when
        // they hit spacebar to close it and accidentally jumped. This way the button press
        // to close the text won't go through the rest of this function, so it won't register
        // a jump.
        if (Input.anyKey && !textClosed)
        {
            openingText.transform.GetChild(0).gameObject.SetActive(false);
            textClosed = true;
            return;
        }

        // This calls a function which will restart the level if the player falls off the screen.
        if (this.transform.position.y <= -6f)
            this.Restart();

        // This closes the game if the player presses escape.
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        // If you died by jumping too much this calls GameOver. GameOver does not immediately reload
        // the level, it fades the screen purple and then reloads it after giving a message. This means
        // you could technically touch the exit sign before it fades all the way but I think this is kind
        // of cool so I consider it a feature.
        if (dead)
        {
            GameOver();
        }

        // This calls the purple fade if either part of it is on (fade up or fade down). Also shouldn't
        // be called when you're dead or it might mess up the fade in GameOver(). 
        if ((jumpFXup || jumpFXdown) && !dead)
        {
            this.JumpFX();
        }

        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        // The next two if statements are both supposed to end an effect when the play touches the ground
        // after a successful jump between levels. I had a bug where sometimes the jump effects wouldn't
        // happen at all, though, and I eventually found out that it was because sometimes the grounded
        // variable from the tutorial can think you're on the ground for a few frames after you jump.
        // They check jumpBufferTime now so they can skip those early frames, and since that change the
        // effect seems to launch consistently.

        // This is easy to miss but it just turns off the camera shake when you hit the ground (but not
        // if you're dead, because that has it's own shake effect.)
        if(grounded && !dead && jumpBufferTime > .2) shake.m_AmplitudeGain = 0f;

        // Stop the jumping noise if the player hits the ground, but not if the player is dead from jumping too
        // much.
        if (grounded && jumpSound.isPlaying && !dead && jumpBufferTime > .2) jumpSound.Stop();

        if (Input.GetButtonDown("Jump") && grounded)
        {
            // This just switches the active grid every time jump is pressed 
            // (assuming you are able to jump.) I had to put it here in particular
            // so it would happen as soon as you jump and you have time to reorient
            // before landing.

            if (past.transform.GetChild(0).gameObject.activeSelf)
            {
                past.transform.GetChild(0).gameObject.SetActive(false);
                present.transform.GetChild(0).gameObject.SetActive(true);

                // This part calculates for collision the same way it would for 
                // a regular physics_object in the original script, except it checks 
                // for 0 distance, so it only cares if you are already colliding with
                // something.

                // If you are colliding with something right after switching the level, 
                // I don't want you to be able to switch levels, so this part just cancels
                // the level switch and gives you a quick purple flash to let you know
                // you can't switch there. 
                int count = rb2d.Cast(move, contactFilter, hitBuffer, 0f);
                if (count > 0)
                {
                    //Play jumpsound for only a second to warn player that they can't jump.

                    jumpSound.time = 1f;
                    jumpSound.Play();
                    jumpSound.SetScheduledEndTime(AudioSettings.dspTime + (1.15f - 1f));

                    past.transform.GetChild(0).gameObject.SetActive(true);
                    present.transform.GetChild(0).gameObject.SetActive(false);
                    fadeSpeed = 10f;
                }

                // So, if you don't collide, we finalize the switch by changing the backgrounds and
                // making the camera shake.
                else
                {
                    // This resets jumpBufferTime so it accurately gives the time between jumps
                    // to the if statements later. The same thing is in the if statement for jumping
                    // the other way, of course.
                    jumpBufferTime = 0;

                    //This plays jumpsound if the player actually jumps.
                    jumpSound.Play();

                    pastBackground.transform.GetChild(1).gameObject.SetActive(false);
                    presentBackground.transform.GetChild(0).gameObject.SetActive(true);
                    presentBackground.transform.GetChild(1).gameObject.SetActive(true);
                    shake.m_AmplitudeGain = 1f;


                    jumpsLeft = jumpsLeft - 1;

                    string jumps = jumpsLeft.ToString();
                    jumpsLeftText.text = string.Format("Jumps Remaining: {0}", jumps);

                    // Self explanatory I hope. If you use more jumps than you have, 
                    // the game is over so dead is true and the jump remaining text
                    // should be invisible so I lazily just made it an empty string.
                    if (jumpsLeft < 0)
                    {
                        jumpsLeftText.text = "";
                        dead = true;
                    }
                }
            }

            // This block isn't commented as much because it's basically identical to the last
            // if statement except this is to switch from present to past instead of the other way.
            // If I was going to continue with this project I would probably make both of these the 
            // same function so it's easier to read, but the inertia of getting everything done has
            // led to some not optimal practices. 

            else
            {
                present.transform.GetChild(0).gameObject.SetActive(false);
                past.transform.GetChild(0).gameObject.SetActive(true);

                // Just like the last one, this checks for collisions and then
                // either lets you switch levels or not accordingly.
                int count = rb2d.Cast(move, contactFilter, hitBuffer, 0f);
                if (count > 0)
                {
                    jumpSound.time = 1f;
                    jumpSound.Play();
                    jumpSound.SetScheduledEndTime(AudioSettings.dspTime + (1.15f - 1f));

                    past.transform.GetChild(0).gameObject.SetActive(false);
                    present.transform.GetChild(0).gameObject.SetActive(true);
                    fadeSpeed = 10f;
                }
                else
                {
                    jumpBufferTime = 0;
                    jumpSound.Play();

                    pastBackground.transform.GetChild(1).gameObject.SetActive(true);
                    presentBackground.transform.GetChild(0).gameObject.SetActive(false);
                    presentBackground.transform.GetChild(1).gameObject.SetActive(false);
                    shake.m_AmplitudeGain = 1f;

                    jumpsLeft = jumpsLeft - 1;

                    string jumps = jumpsLeft.ToString();
                    jumpsLeftText.text = string.Format("Jumps Remaining: {0}", jumps);

                    if (jumpsLeft < 0)
                    {
                        jumpsLeftText.text = "";
                        dead = true;
                    }
                }
            }

            // This activates jumpFXup which will turn on the purple effect.
            jumpFXup = true;

            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * .5f;
            }
        }


        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX; 
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);


        targetVelocity = move * maxSpeed;
    }
}
